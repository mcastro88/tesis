/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author DeahtStar
 */
public class Parametro {
    private int idParametro;
    private int rangoMinimo;
    private int rangoMaximo;
    private int valor;
    private int idAnalisis;
    private int idSubcategoria;
    private String nombre;
    private String unidad;
    
    public Parametro (int idParametro, int rangoMax, int rangoMin,
                      int valor, int idAnalisis, int idSubcategoria,
                      String nombre, String unidad){
        
        this.idAnalisis = idAnalisis;
        this.idParametro = idParametro;
        this.idSubcategoria = idSubcategoria;
        this.nombre = nombre;
        this.rangoMaximo = rangoMax;
        this.rangoMinimo = rangoMin;
        this.unidad = unidad;
        this.valor = valor;
        
    }
    /**
     * @return the idParametro
     */
    public int getIdParametro() {
        return idParametro;
    }

    /**
     * @param idParametro the idParametro to set
     */
    public void setIdParametro(int idParametro) {
        this.idParametro = idParametro;
    }

    /**
     * @return the rangoMinimo
     */
    public int getRangoMinimo() {
        return rangoMinimo;
    }

    /**
     * @param rangoMinimo the rangoMinimo to set
     */
    public void setRangoMinimo(int rangoMinimo) {
        this.rangoMinimo = rangoMinimo;
    }

    /**
     * @return the rangoMaximo
     */
    public int getRangoMaximo() {
        return rangoMaximo;
    }

    /**
     * @param rangoMaximo the rangoMaximo to set
     */
    public void setRangoMaximo(int rangoMaximo) {
        this.rangoMaximo = rangoMaximo;
    }

    /**
     * @return the valor
     */
    public int getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(int valor) {
        this.valor = valor;
    }

    /**
     * @return the idAnalisis
     */
    public int getIdAnalisis() {
        return idAnalisis;
    }

    /**
     * @param idAnalisis the idAnalisis to set
     */
    public void setIdAnalisis(int idAnalisis) {
        this.idAnalisis = idAnalisis;
    }

    /**
     * @return the idSubcategoria
     */
    public int getIdSubcategoria() {
        return idSubcategoria;
    }

    /**
     * @param idSubcategoria the idSubcategoria to set
     */
    public void setIdSubcategoria(int idSubcategoria) {
        this.idSubcategoria = idSubcategoria;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the unidad
     */
    public String getUnidad() {
        return unidad;
    }

    /**
     * @param unidad the unidad to set
     */
    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }
}
