/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author DeahtStar
 */
public class Atencion {
    private int idAtencion;
    private int idPaciente;
    private int idProfesional;
    private String fecha;
    
    public Atencion (int idAtencion, int idPaciente, int idProfesional,
                    String fecha){
        this.fecha = fecha;
        this.idAtencion = idAtencion;
        this.idPaciente = idPaciente;
        this.idProfesional = idProfesional;
    }
    /**
     * @return the idAtencion
     */
    public int getIdAtencion() {
        return idAtencion;
    }

    /**
     * @param idAtencion the idAtencion to set
     */
    public void setIdAtencion(int idAtencion) {
        this.idAtencion = idAtencion;
    }

    /**
     * @return the idPaciente
     */
    public int getIdPaciente() {
        return idPaciente;
    }

    /**
     * @param idPaciente the idPaciente to set
     */
    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    /**
     * @return the idProfesional
     */
    public int getIdProfesional() {
        return idProfesional;
    }

    /**
     * @param idProfesional the idProfesional to set
     */
    public void setIdProfesional(int idProfesional) {
        this.idProfesional = idProfesional;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
