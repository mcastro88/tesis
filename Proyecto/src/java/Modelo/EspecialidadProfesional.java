/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author DeahtStar
 */
public class EspecialidadProfesional {
    private int idEspProfesional;
    private int idEspecialidad;
    private int idProfesional;

    /**
     * @return the idEspProfesional
     */
    public int getIdEspProfesional() {
        return idEspProfesional;
    }

    /**
     * @param idEspProfesional the idEspProfesional to set
     */
    public void setIdEspProfesional(int idEspProfesional) {
        this.idEspProfesional = idEspProfesional;
    }

    /**
     * @return the idEspecialidad
     */
    public int getIdEspecialidad() {
        return idEspecialidad;
    }

    /**
     * @param idEspecialidad the idEspecialidad to set
     */
    public void setIdEspecialidad(int idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    /**
     * @return the idProfesional
     */
    public int getIdProfesional() {
        return idProfesional;
    }

    /**
     * @param idProfesional the idProfesional to set
     */
    public void setIdProfesional(int idProfesional) {
        this.idProfesional = idProfesional;
    }
}
