/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author DeahtStar
 */
public class Analisis {
    
    private int idAnalisis;
    private int idTipoAnalisis;
    private int idAtencion;
    private String NrodeOrden;
    private String nombre;
    private String nombreProfesional;
    private String observacion;
    private String entidad;
    private String fecha;
    
    public Analisis (int idAnalisis,int idTipoAnalisis,int idAtencion,
                     String nrodeOrden,String nombre,String nombreProf,
                     String observacion,String entidad,String fecha){
        
        this.idAnalisis = idAnalisis;
        this.idAtencion = idAtencion;
        this.idTipoAnalisis = idTipoAnalisis;
        this.NrodeOrden = nrodeOrden;
        this.entidad = entidad;
        this.fecha = fecha;
        this.nombre = nombre;
        this.nombreProfesional = nombreProf;
        this.observacion = observacion;
        
    }
    /**
     * @return the idAnalisis
     */
     
    public int getIdAnalisis() {
        return idAnalisis;
    }

    /**
     * @param idAnalisis the idAnalisis to set
     */
    public void setIdAnalisis(int idAnalisis) {
        this.idAnalisis = idAnalisis;
    }

    /**
     * @return the NrodeOrden
     */
    public String getNrodeOrden() {
        return NrodeOrden;
    }

    /**
     * @param NrodeOrden the NrodeOrden to set
     */
    public void setNrodeOrden(String NrodeOrden) {
        this.NrodeOrden = NrodeOrden;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the nombreProfesional
     */
    public String getNombreProfesional() {
        return nombreProfesional;
    }

    /**
     * @param nombreProfesional the nombreProfesional to set
     */
    public void setNombreProfesional(String nombreProfesional) {
        this.nombreProfesional = nombreProfesional;
    }

    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * @param entidad the entidad to set
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the idTipoAnalisis
     */
    public int getIdTipoAnalisis() {
        return idTipoAnalisis;
    }

    /**
     * @param idTipoAnalisis the idTipoAnalisis to set
     */
    public void setIdTipoAnalisis(int idTipoAnalisis) {
        this.idTipoAnalisis = idTipoAnalisis;
    }

    /**
     * @return the idAtencion
     */
    public int getIdAtencion() {
        return idAtencion;
    }

    /**
     * @param idAtencion the idAtencion to set
     */
    public void setIdAtencion(int idAtencion) {
        this.idAtencion = idAtencion;
    }
    
}
