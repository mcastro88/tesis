/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author DeahtStar
 */
public class TipoAnalisis {
    private int idTipoAnalisis;
    private String nombre;

    /**
     * @return the idTipoAnalisis
     */
    public int getIdTipoAnalisis() {
        return idTipoAnalisis;
    }

    /**
     * @param idTipoAnalisis the idTipoAnalisis to set
     */
    public void setIdTipoAnalisis(int idTipoAnalisis) {
        this.idTipoAnalisis = idTipoAnalisis;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
