/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author DeahtStar
 */
public class Profesional {
    private int idProfesional;
    private int idLogin;
    private String nombre;
    private String apellido;
    private String matricula;
    private String telefono;
    private String email;
    
    public Profesional (int idProfesional, int idLogin,String nombre,
                        String apellido, String matricula, String tel,
                        String email){
        this.apellido = apellido;
        this.email = email;
        this.idLogin = idLogin;
        this.idProfesional = idProfesional;
        this.matricula = matricula;
        this.nombre = nombre;
        this.telefono = tel;
    }
    /**
     * @return the idProfesional
     */
    public int getIdProfesional() {
        return idProfesional;
    }

    /**
     * @param idProfesional the idProfesional to set
     */
    public void setIdProfesional(int idProfesional) {
        this.idProfesional = idProfesional;
    }

    /**
     * @return the idLogin
     */
    public int getIdLogin() {
        return idLogin;
    }

    /**
     * @param idLogin the idLogin to set
     */
    public void setIdLogin(int idLogin) {
        this.idLogin = idLogin;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
