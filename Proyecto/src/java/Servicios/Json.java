package Servicios;


import com.google.gson.Gson;
import java.util.*;

public class Json {
       
    public Gson aJson (ArrayList<Object> lista){
        
        Gson listaGson = new Gson();
        listaGson.toJson(lista);
        
        return listaGson;
    }
}
