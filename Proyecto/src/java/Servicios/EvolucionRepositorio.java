
package Servicios;


import java.sql.SQLException;
import java.sql.ResultSet;
import Modelo.Paciente;
import Modelo.Analisis;
import java.util.ArrayList;

public class EvolucionRepositorio {
    
    public ArrayList<Paciente> getPacientes (){
        
        ArrayList<Paciente> listaPacientes = new ArrayList();
        ResultSet consulta = null;
        Conexion conect = new Conexion();
        Paciente a = null;
        try{
            consulta = conect.conectar().createStatement().executeQuery("SELECT * FROM paciente;");
            while (consulta.next()){
                try {
                    String apellido = consulta.getString("Apellido");
                    String dni = consulta.getString("DNI");
                    String email = consulta.getString("Email");
                    String fechaNacimiento = consulta.getString("FechaNacimiento");
                    int idLogin = Integer.parseInt(consulta.getString("idLogin")) ;
                    int idPaciente = Integer.parseInt(consulta.getString("idPaciente")) ;;
                    String nacionalidad = consulta.getString("Nacionalidad") ;
                    String nombre = consulta.getString("Nombre");
                    String sexo = consulta.getString("Sexo");
                    String telefono = consulta.getString("Telefono");
                    
                    a = new Paciente(idPaciente, idLogin, dni, nombre, apellido, 
                                    fechaNacimiento, sexo, telefono, email,
                                    nacionalidad);
                    
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                listaPacientes.add(a);
            }
        }catch (SQLException ex){
            System.out.println(ex);
        }
        
        
        
        return listaPacientes;
    }
    
    public ResultSet getPaciente (int id){
                
        ResultSet consulta = null;
        Conexion conect = new Conexion();
        
        try{
            consulta = conect.conectar().createStatement().executeQuery("SELECT * FROM paciente WHERE IdPaciente =;"+id);
        }catch (SQLException ex){
            System.out.println(ex);
        }
        return consulta;
    }
    
    public ResultSet getProfesional (int id){
        
        ResultSet consulta = null;
        Conexion conect = new Conexion();
        
        try{
            consulta = conect.conectar().createStatement().executeQuery("SELECT * FROM profesional;");
        }catch (SQLException ex){
            System.out.println(ex);
        }
        return consulta;
    }
    
    public ArrayList<Analisis> getAnalisis (){
        
        ArrayList<Analisis> listaAnalisis = new ArrayList ();
        ResultSet consulta = null;
        Conexion conect = new Conexion();
        Analisis a = null;
        
        try{
            consulta = conect.conectar().createStatement().executeQuery("SELECT a.IdAnalisis, a.IdTipoAnalisis, a.Nombre,"
                    + "                                                  a.IdAtencion, a.NombreProfesional, a.Observacion, a.Fecha,"
                    + "                                                  a.Entidad, a.NroOrden, tipoA.Nombre,  FROM analisis AS a"
                    + "                                                  INNER JOIN tipoanalisis AS tipoA"
                    + "                                                  ON tipoA.IdTipoAnalisis = a.IdTipoAnalisis"
                    + "                                                  INNER JOIN atencion AS aten"
                    + "                                                  ON aten.IdAtencion = a.IdAtencion"
                    + "                                                  INNER JOIN paciente AS p"
                    + "                                                  ON p.IdPaciente = aten.IdPaciente;");
            while (consulta.next()){
                try {
                    String entidad = consulta.getString("Entidad");
                    String fecha = consulta.getString("Fecha");
                    int idTipoAnalisis = Integer.parseInt(consulta.getString("idTipoAnalisis"));
                    int idAtencion = Integer.parseInt(consulta.getString("idAtencion"));
                    int idAnalisis = Integer.parseInt(consulta.getString("idAnalisis"));
                    String nombreProfesional = consulta.getString("NombreProfesional");
                    String nombre = consulta.getString("Nombre");
                    String nroOrden = consulta.getString("NroOrden");
                    String observacion = consulta.getString("Observacion");
                    
                    a = new Analisis(idAnalisis, idTipoAnalisis, idAtencion,
                                    nroOrden, nombre, nombreProfesional, 
                                    observacion, entidad, fecha);
                    
                }catch (Exception ex){
                   ex.printStackTrace();
                }
                listaAnalisis.add(a);
            }
        
        }catch (SQLException ex){
            System.out.println(ex);
        }
        return listaAnalisis;
    }
    
    public ResultSet getDatosCabecera (int idPaciente){
        
        ResultSet consulta = null;
        Conexion conect = new Conexion();
        try{
        consulta = conect.conectar().createStatement().executeQuery("SELECT"
                                                                      + "(p.Nombre + ' ' + p.Apellido) as Nombre,"
                                                                      + "p.FechaNacimiento as FechaNacimiento"
                                                                  + "FROM paciente p"
                                                                  + "WHERE p.IdPaciente = " + idPaciente + "");
        }catch (SQLException ex){
            System.out.println(ex);
        }
        return consulta;
    }
    
    
    
}
