package Servicios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    
    private String url, usuario, clave;
    
    public Conexion (){
        
        this.url = "jdbc:mysql://localhost/cyamp";
        this.clave = "";
        this.usuario = "root";
    }
    
    public void setUrl(String url) {
        
        this.url = url;
    }
    
    public void setUsuario(String usuario) {
          
        this.usuario = usuario;
    }
    
    public void setClave(String clave) {
       
        this.clave = clave;
    }
    
    public Connection conectar(){
        
        Connection conec = null;
        
        try{
            conec = DriverManager.getConnection(url, usuario, clave);
        }
        catch (SQLException ex){
            System.out.println ("Clase conexion, metodo conectar:\n"+ex);
        }
        return conec;
    }
    
    
}
