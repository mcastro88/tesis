-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-08-2018 a las 04:32:35
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cyamp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `analisis`
--

CREATE TABLE IF NOT EXISTS `analisis` (
`IdAnalisis` int(11) NOT NULL,
  `IdTipoAnalisis` int(11) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `IdAtencion` int(11) NOT NULL,
  `NombreProfesional` varchar(30) NOT NULL,
  `Observacion` varchar(500) NOT NULL,
  `Fecha` date NOT NULL,
  `Entidad` varchar(100) NOT NULL,
  `NroOrden` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `analisis`
--

INSERT INTO `analisis` (`IdAnalisis`, `IdTipoAnalisis`, `Nombre`, `IdAtencion`, `NombreProfesional`, `Observacion`, `Fecha`, `Entidad`, `NroOrden`) VALUES
(1, 1, 'Control Rutina', 1, 'Dra Florencia Cardozo', 'Examen periódico semestral', '2018-08-29', 'Centro Médico Silvio Soldan', '1408');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `atencion`
--

CREATE TABLE IF NOT EXISTS `atencion` (
`IdAtencion` int(11) NOT NULL,
  `IdPaciente` int(11) NOT NULL,
  `IdProfesional` int(11) NOT NULL,
  `FechaApertura` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `atencion`
--

INSERT INTO `atencion` (`IdAtencion`, `IdPaciente`, `IdProfesional`, `FechaApertura`) VALUES
(1, 8, 1, '2018-08-29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
`IdCategoria` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`IdCategoria`, `Nombre`) VALUES
(1, 'Hematología y Hemostasia'),
(2, 'Química'),
(3, 'Orina');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidad`
--

CREATE TABLE IF NOT EXISTS `especialidad` (
`IdEspecialidad` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidadprofesional`
--

CREATE TABLE IF NOT EXISTS `especialidadprofesional` (
`IdEspProfesional` int(11) NOT NULL,
  `IdEspecialidad` int(11) NOT NULL,
  `IdProfesional` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`IdLogin` int(11) NOT NULL,
  `IdRol` int(11) NOT NULL,
  `FechaAlta` date NOT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'0',
  `Usuario` varchar(100) NOT NULL,
  `Clave` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`IdLogin`, `IdRol`, `FechaAlta`, `Activo`, `Usuario`, `Clave`) VALUES
(1, 1, '2018-08-29', b'1', 'Pedro.Moran', '1234'),
(3, 2, '2018-08-29', b'1', 'Juan.Perez', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE IF NOT EXISTS `paciente` (
`IdPaciente` int(11) NOT NULL,
  `DNI` varchar(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Apellido` varchar(100) NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `Telefono` varchar(200) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Nacionalidad` varchar(30) DEFAULT NULL,
  `IdLogin` int(11) NOT NULL,
  `Sexo` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`IdPaciente`, `DNI`, `Nombre`, `Apellido`, `FechaNacimiento`, `Telefono`, `Email`, `Nacionalidad`, `IdLogin`, `Sexo`) VALUES
(8, '23078552', 'Juan', 'Perez', '1978-04-29', NULL, 'juan.perez@gmail.com', 'Argentina', 3, 'M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametro`
--

CREATE TABLE IF NOT EXISTS `parametro` (
`IdParametro` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `RangoMinimo` float DEFAULT NULL,
  `RangoMaximo` float DEFAULT NULL,
  `Valor` float NOT NULL,
  `IdAnalisis` int(11) NOT NULL,
  `Unidad` varchar(10) DEFAULT NULL,
  `IdSubCategoria` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parametro`
--

INSERT INTO `parametro` (`IdParametro`, `Nombre`, `RangoMinimo`, `RangoMaximo`, `Valor`, `IdAnalisis`, `Unidad`, `IdSubCategoria`) VALUES
(1, 'Recuento de glóbulos rojos', 388, 499, 485, 1, 'x10000 mm3', 1),
(2, 'Hematocrito', 39, 50, 39.8, 1, '%', 1),
(3, 'Dosaje de Hemoglobina', 11.8, 14.8, 13.2, 1, 'g/dl', 1),
(4, 'Volumen corpuscular medio', 81, 99, 82, 1, 'fL', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesional`
--

CREATE TABLE IF NOT EXISTS `profesional` (
`IdProfesional` int(11) NOT NULL,
  `Matricula` varchar(10) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Apellido` varchar(50) NOT NULL,
  `Telefono` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `IdLogin` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profesional`
--

INSERT INTO `profesional` (`IdProfesional`, `Matricula`, `Nombre`, `Apellido`, `Telefono`, `Email`, `IdLogin`) VALUES
(1, 'MN-3308', 'Pedro', 'Moran', NULL, 'pedro.moran@medife.com.ar', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE IF NOT EXISTS `rol` (
`IdRol` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`IdRol`, `Nombre`) VALUES
(1, 'Profesional'),
(2, 'Paciente'),
(3, 'Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria`
--

CREATE TABLE IF NOT EXISTS `subcategoria` (
`IdSubCategoria` int(11) NOT NULL,
  `IdCategoria` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subcategoria`
--

INSERT INTO `subcategoria` (`IdSubCategoria`, `IdCategoria`, `Nombre`) VALUES
(1, 1, 'Hemograma'),
(2, 1, 'Recuento de plaquetas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoanalisis`
--

CREATE TABLE IF NOT EXISTS `tipoanalisis` (
`IdTipoAnalisis` int(11) NOT NULL,
  `Nombre` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoanalisis`
--

INSERT INTO `tipoanalisis` (`IdTipoAnalisis`, `Nombre`) VALUES
(1, 'Rutina');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `analisis`
--
ALTER TABLE `analisis`
 ADD PRIMARY KEY (`IdAnalisis`), ADD KEY `IdTipoAnalisis` (`IdTipoAnalisis`), ADD KEY `IdAtencion` (`IdAtencion`);

--
-- Indices de la tabla `atencion`
--
ALTER TABLE `atencion`
 ADD PRIMARY KEY (`IdAtencion`), ADD KEY `IdPaciente` (`IdPaciente`), ADD KEY `IdProfesional` (`IdProfesional`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
 ADD PRIMARY KEY (`IdCategoria`);

--
-- Indices de la tabla `especialidad`
--
ALTER TABLE `especialidad`
 ADD PRIMARY KEY (`IdEspecialidad`);

--
-- Indices de la tabla `especialidadprofesional`
--
ALTER TABLE `especialidadprofesional`
 ADD PRIMARY KEY (`IdEspProfesional`), ADD KEY `IdEspecialidad` (`IdEspecialidad`), ADD KEY `IdProfesional` (`IdProfesional`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`IdLogin`), ADD KEY `IdRol` (`IdRol`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
 ADD PRIMARY KEY (`IdPaciente`), ADD KEY `IdLogin` (`IdLogin`);

--
-- Indices de la tabla `parametro`
--
ALTER TABLE `parametro`
 ADD PRIMARY KEY (`IdParametro`), ADD KEY `IdAnalisis` (`IdAnalisis`), ADD KEY `IdSubCategoria` (`IdSubCategoria`);

--
-- Indices de la tabla `profesional`
--
ALTER TABLE `profesional`
 ADD PRIMARY KEY (`IdProfesional`), ADD KEY `IdLogin` (`IdLogin`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
 ADD PRIMARY KEY (`IdRol`);

--
-- Indices de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
 ADD PRIMARY KEY (`IdSubCategoria`), ADD KEY `IdCategoria` (`IdCategoria`);

--
-- Indices de la tabla `tipoanalisis`
--
ALTER TABLE `tipoanalisis`
 ADD PRIMARY KEY (`IdTipoAnalisis`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `analisis`
--
ALTER TABLE `analisis`
MODIFY `IdAnalisis` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `atencion`
--
ALTER TABLE `atencion`
MODIFY `IdAtencion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
MODIFY `IdCategoria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `especialidad`
--
ALTER TABLE `especialidad`
MODIFY `IdEspecialidad` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `especialidadprofesional`
--
ALTER TABLE `especialidadprofesional`
MODIFY `IdEspProfesional` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `login`
--
ALTER TABLE `login`
MODIFY `IdLogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `paciente`
--
ALTER TABLE `paciente`
MODIFY `IdPaciente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `parametro`
--
ALTER TABLE `parametro`
MODIFY `IdParametro` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `profesional`
--
ALTER TABLE `profesional`
MODIFY `IdProfesional` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
MODIFY `IdRol` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
MODIFY `IdSubCategoria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipoanalisis`
--
ALTER TABLE `tipoanalisis`
MODIFY `IdTipoAnalisis` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `analisis`
--
ALTER TABLE `analisis`
ADD CONSTRAINT `analisis_ibfk_1` FOREIGN KEY (`IdTipoAnalisis`) REFERENCES `tipoanalisis` (`IdTipoAnalisis`);

--
-- Filtros para la tabla `atencion`
--
ALTER TABLE `atencion`
ADD CONSTRAINT `ATENCION_ibfk_1` FOREIGN KEY (`IdProfesional`) REFERENCES `profesional` (`IdProfesional`),
ADD CONSTRAINT `ATENCION_ibfk_2` FOREIGN KEY (`IdPaciente`) REFERENCES `paciente` (`IdPaciente`);

--
-- Filtros para la tabla `especialidadprofesional`
--
ALTER TABLE `especialidadprofesional`
ADD CONSTRAINT `ESPECIALIDADPROFESIONAL_ibfk_1` FOREIGN KEY (`IdEspecialidad`) REFERENCES `especialidad` (`IdEspecialidad`);

--
-- Filtros para la tabla `login`
--
ALTER TABLE `login`
ADD CONSTRAINT `LOGIN_ibfk_1` FOREIGN KEY (`IdLogin`) REFERENCES `rol` (`IdRol`);

--
-- Filtros para la tabla `parametro`
--
ALTER TABLE `parametro`
ADD CONSTRAINT `parametro_ibfk_1` FOREIGN KEY (`IdSubCategoria`) REFERENCES `subcategoria` (`IdSubCategoria`),
ADD CONSTRAINT `parametro_ibfk_2` FOREIGN KEY (`IdAnalisis`) REFERENCES `analisis` (`IdAnalisis`);

--
-- Filtros para la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
ADD CONSTRAINT `SUBCATEGORIA_ibfk_1` FOREIGN KEY (`IdCategoria`) REFERENCES `categoria` (`IdCategoria`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
